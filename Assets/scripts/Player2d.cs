﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2d : MonoBehaviour
{
    Rigidbody2D rb;
    Vector2 velocity;
    public int score = 0;
    public string PlayerName = "";
    public float speed = 10;
    public Text nameLabel;
    public Text coinsLabel;

    private int oldScore;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        nameLabel.text = PlayerName;
    }

    // Update is called once per frame
    void Update()
    {
        velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized * speed;
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);

        if (oldScore != score) {
            coinsLabel.text = score.ToString();
            oldScore = score;
        }
    }
}
