﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinScript : MonoBehaviour
{
    void Awake()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject player = collision.attachedRigidbody.gameObject;
        Player2d playerScript = player.GetComponent<Player2d>();
        playerScript.score += 10;
        Debug.Log(playerScript.PlayerName + " : " + playerScript.score.ToString());
        Destroy(this.gameObject);
    }

    void Update()
    {
        
    }
}
