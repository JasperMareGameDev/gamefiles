﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MapGenConstants
{
    public static ushort maxTilesX = 24;
    public static ushort maxTilesY = 24;

    public enum TileID {
        ground = 00,
        orangeWall = 01,
        blueWall = 02,
        pinkWall = 03,
        purpleWall = 04,
        orangeFlag = 05,
        blueFlag = 06,
        pinkFlag = 07,
        purpleFlag = 08,
        orangeSpawn = 09,
        blueSpawn = 10,
        pinkSpawn = 11,
        purpleSpawn = 12,
        coinSpawn = 13,
        bombSpawns = 14
    }

}