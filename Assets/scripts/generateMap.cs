﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static MapGenConstants;

public class generateMap : MonoBehaviour
{
    public TextAsset TextFile;
    TileID[,] map = new TileID[maxTilesX,maxTilesY];
    public Sprite texture_ground;
    public Sprite texture_orangeWall;
    public Sprite texture_blueWall;
    public Sprite texture_pinkWall;
    public Sprite texture_purpleWall;
    public Sprite texture_orangeFlag;
    public Sprite texture_blueFlag;
    public Sprite texture_pinkFlag;
    public Sprite texture_purpleFlag;
    public Sprite texture_orangeSpawn;
    public Sprite texture_blueSpawn;
    public Sprite texture_pinkSpawn;
    public Sprite texture_purpleSpawn;
    public Sprite texture_coinSpawn;
    public Sprite texture_bombspawn;

    public Sprite texture_coins;

    private int Top = -maxTilesY / 2;
    private int Left = -maxTilesX / 2;

    void Start()
    {
        fillAllTiles(TileID.ground); // clear
        if (TextFile != null) //read map from csv
        {
            try
            {
                string[] values = new string[maxTilesX * maxTilesY];
                values = TextFile.text.Split(',');
                int i = 0;
                for (int x = 0; x < maxTilesX; x++)
                {
                    for (int y = 0; y < maxTilesY; y++)
                    {
                        if (Enum.IsDefined(typeof(TileID), (TileID)int.Parse(values[i])))
                        {
                            map[x, y] = (TileID)int.Parse(values[i]); // put tile from map onto 2d array
                        }
                        else
                        {
                            map[x, y] = TileID.ground; //if it is an unimplemented value, replace with ground
                        }
                        spawntile(x, y, map[x, y]); // spawn tile into game
                        i++;
                    }
                }

            }
            catch { Debug.LogError("error loading map file, check for un-accepted charecters, or missing commas"); }
        }
        else { Debug.LogError("attach a map to the generator"); }
        
        // rotate map to fix orientation problem
        this.transform.Rotate(0, 0, 270);
        this.transform.position = new Vector2(-(maxTilesX/2) + 0.5f, maxTilesY/2 - 0.5f);
        //spawn coins
        if (TextFile != null) { //can't spawn coins if there is no textfile
            for (int x = 0; x < maxTilesX; x++)
            {
                for (int y = 0; y < maxTilesY; y++)
                {
                    if (map[x, y] == TileID.coinSpawn) {
                        GameObject coin = new GameObject("Coin - X: " + x + "Y: " + y);
                        Vector3 p;
                        p.x = convertMapPosToGamePos(x, y).x;
                        p.y = convertMapPosToGamePos(x, y).y;
                        p.z = -0.1f;
                        coin.transform.position = p;
                        coin.transform.parent = this.transform;
                        var s = coin.AddComponent<SpriteRenderer>();
                        s.sprite = texture_coins;
                        var collider = coin.AddComponent<BoxCollider2D>();
                        collider.isTrigger = true;
                        coin.AddComponent(Type.GetType("coinScript"));
                    }
                }
            }
        }
    }

    void fillAllTiles(TileID tile) {
        for (int x = 0; x < maxTilesX; x++) {
            for (int y = 0; y < maxTilesY; y++) {
                map[x, y] = tile;
            }
        }
    }

    Vector2 convertMapPosToGamePos(float x, float y) {
        x += this.transform.position.x;
        y -= this.transform.position.y;
        return new Vector3(x, y);
    }

    void spawntile(int x, int y, TileID tileType) {
        GameObject tile = new GameObject("X: " + x + "Y: " + y);
        tile.transform.position = new Vector2(x, y);
        tile.transform.parent = this.transform;
        var s = tile.AddComponent<SpriteRenderer>();
        switch (tileType) {
            case TileID.ground:
                s.sprite = texture_ground;
                break;
            case TileID.orangeWall:
                s.sprite = texture_orangeWall;
                tile.AddComponent<BoxCollider2D>();
                break;
            case TileID.blueWall:
                s.sprite = texture_blueWall;
                tile.AddComponent<BoxCollider2D>();
                break;
            case TileID.pinkWall:
                s.sprite = texture_pinkWall;
                tile.AddComponent<BoxCollider2D>();
                break;
            case TileID.purpleWall:
                s.sprite = texture_purpleWall;
                tile.AddComponent<BoxCollider2D>();
                break;
            case TileID.orangeFlag:
                s.sprite = texture_orangeFlag;
                break;
            case TileID.blueFlag:
                s.sprite = texture_blueFlag;
                break;
            case TileID.pinkFlag:
                s.sprite = texture_pinkFlag;
                break;
            case TileID.purpleFlag:
                s.sprite = texture_purpleFlag;
                break;
            case TileID.orangeSpawn:
                s.sprite = texture_orangeSpawn;
                break;
            case TileID.blueSpawn:
                s.sprite = texture_blueSpawn;
                break;
            case TileID.pinkSpawn:
                s.sprite = texture_pinkSpawn;
                break;
            case TileID.purpleSpawn:
                s.sprite = texture_purpleSpawn;
                break;
            case TileID.coinSpawn:
                s.sprite = texture_coinSpawn;
                break;
            case TileID.bombSpawns:
                s.sprite = texture_bombspawn;
                break;
        } //per tile type proccessing (i.e textures, colliders)
        tile.transform.Rotate(0, 0, 90);
    }

}
